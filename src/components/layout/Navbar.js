import React, { Component } from 'react'
import Mountain from '../../mountain.png';

export class Navbar extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-dark bg-dark">
                    <a className="navbar-brand" href="/">
                        <img src={Mountain} width="45" height="45" alt=""></img>
                        Fahrradtour
                    </a>
                </nav>
            </div>
        )
    }
}

export default Navbar
