import React from "react";
import "../index.css";
import PropTypes from "prop-types";
import DeleteRow from "./DeleteRow";
import AddRow from "./AddRow";
import UpdateRow from "./UpdateRow";

export default class LegTable extends React.Component {

  render() {
    if (this.props.legs.length > 0) {
      return (
        <div className="table-responsive">
          <table className="table table-dark">
            <thead>
              <tr>
                <th>Nr.</th>
                <th>Beschreibung</th>
                <th>Lat.</th>
                <th>Lng.</th>
                <th>Übernachtung</th>
                <th>Begleitfahrzeug</th>
                <th>Fahrer</th>
                <th>Bearbeiten</th>
              </tr>
            </thead>
            <tbody>
              {this.props.legs.map((leg) => (
                <tr key={leg.id}>
                  <td>{leg.number}</td>
                  <td>{leg.description}</td>
                  <td>{leg.latitude}</td>
                  <td>{leg.longitude}</td>
                  <td>{leg.overnightStay}</td>
                  <td>{leg.supportVehicle}</td>
                  <td>{leg.driver}</td>
                  <td>
                    <AddRow number={leg.number - 1} addLeg={this.props.addLeg}/>
                    <UpdateRow leg={leg} updateLeg={this.props.updateLeg} />
                    <DeleteRow legId={leg.id} deleteLeg={this.props.deleteLeg} />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
    } else {
      return (
        <div className="text-center m-3">
          <p>Es sind noch keine Etappen angelegt</p>
          <AddRow number={1} addLeg={this.props.addLeg}/>
        </div>
      );
    }
      
  }
}

LegTable.propTypes = {
  legs: PropTypes.array.isRequired
}
