import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export default class AddRow extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      description: "",
      latitude: "",
      longitude: "",
      number: props.number,
      overnightStay: "",
      supportVehicle: "",
      driver: "",
    };
  }

  handleChange(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  render() {
    return (
      <div style={{ float: 'left' }}>
        <FontAwesomeIcon icon={faPlus} data-toggle="modal" data-target="#exampleModalCenter" style={{ cursor: 'pointer' }} />

        <div className="modal fade text-left" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div className="modal-content bg-dark">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">Etappe hinzufügen</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" className="text-light">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="form-group">
                    <label htmlFor="description">Beschreibung</label>
                    <input type="text" 
                      className="form-control" 
                      id="description" 
                      placeholder="Beschreibung" 
                      value={this.state.description}
                      onChange={this.handleChange}></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="latitude">Breitengrad</label>
                    <input type="text" 
                      className="form-control" 
                      id="latitude" 
                      placeholder="Breitengrad"
                      value={this.state.latitude}
                      onChange={this.handleChange}></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="longitude">Breitengrad</label>
                    <input type="text" 
                      className="form-control" 
                      id="longitude" 
                      placeholder="Längengrad"
                      value={this.state.longitude}
                      onChange={this.handleChange}></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="overnightStay">Übernachtung</label>
                    <input type="text" 
                      className="form-control" 
                      id="overnightStay" 
                      placeholder="Übernachtung"
                      value={this.state.overnightStay}
                      onChange={this.handleChange}></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="supportVehicle">Begleitfahrzeug</label>
                    <input type="text" 
                      className="form-control" 
                      id="supportVehicle" 
                      placeholder="Begleitfahrzeug"
                      value={this.state.supportVehicle}
                      onChange={this.handleChange}></input>
                  </div>
                  <div className="form-group">
                    <label htmlFor="driver">Fahrer</label>
                    <input type="text" 
                      className="form-control" 
                      id="driver" 
                      placeholder="Fahrer"
                      value={this.state.driver}
                      onChange={this.handleChange}></input>
                  </div>
                  <div>
                    <button type="button" className="btn btn-secondary mr-3" data-dismiss="modal">Abbrechen</button>
                    <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this.props.addLeg.bind(this, this.state)}>Speichern</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
