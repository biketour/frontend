import React, { Component } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import "../index.css";
import PropTypes from "prop-types";

export class GoogleMaps extends Component {
  constructor(props) {
    super(props);

    this.state = {
      waypoints: [],
      travelMode: null,
      origin: [
        parseFloat(
          this.props.legs[0] ? this.props.legs[0].latitude : 47.6616666
        ),
        parseFloat(this.props.legs[0] ? this.props.legs[0].longitude : 9.4794),
      ],
      destination: null,
    };
    this.handleAddMarker = this.handleAddMarker.bind(this);
  }

  componentDidMount() {}

  createMarkers() {
    let table = [];
    this.props.legs.forEach(function (item, index) {
      table.push(
        <Marker
          key={item.id}
          label={index.toString()}
          defaultIcon={null}
          position={{
            lat: parseFloat(item.latitude),
            lng: parseFloat(item.longitude),
          }}
        />
      );
    });
    return table;
  }

  handleAddMarker = (event, map, coord) => {
    console.log();
    let args = {
      longitude: parseFloat(coord.latLng.lng()).toFixed(3),
      latitude: parseFloat(coord.latLng.lat()).toFixed(3),
      description: "-",
      number: this.props.legs.length + 1,
      overnightStay: "-",
      supportVehicle: "-",
      driver: "-",
    };

    this.props.addLeg(args);
  };

  render() {
    return (
      <div>
        <Map
          style={{ height: "94%" }}
          google={this.props.google}
          zoom={8}
          onClick={this.handleAddMarker}
          initialCenter={{
            lat: this.state.origin[0],
            lng: this.state.origin[1],
          }}
        >
          {this.createMarkers()}
        </Map>
      </div>
    );
  }
}

GoogleMaps.propTypes = {
  legs: PropTypes.array.isRequired,
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyDG1nQkiYpMG2NxvnyL9OKRwzP7BlD5S_k",
})(GoogleMaps);
