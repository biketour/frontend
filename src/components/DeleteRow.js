import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";


export default class DeleteRow extends React.Component {

  render() {
    return <FontAwesomeIcon icon={faTrashAlt} onClick={this.props.deleteLeg.bind(this, this.props.legId)} style={{cursor: 'pointer', marginLeft: '5px'}}/>
  }

}

DeleteRow.propTypes = {
  legId: PropTypes.number.isRequired
}
