import React from 'react';
import './App.css';
import Navbar from './components/layout/Navbar';
import LegTable from './components/LegTable';
import GoogleMaps from './components/GoogleMaps';
import * as legService from './services/leg-service';

export default class App extends React.Component {

  state = {
    error: null,
    isLoaded: false,
    legs: [],
  }

  componentDidMount() {
    this.getAllLegs();
  }

  getAllLegs() {
    legService.getAllLegs()
      .then((res) => res.json())
      .then(
        (result) => {
          result = result.sort(this.compare);
          this.setState({
            isLoaded: true,
            legs: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  deleteLeg = (legId) => {
    legService.deleteLeg(legId)
      .then(async (response) => {
        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          //const error = (response && response.message) || response.status;
          // return Promise.reject(error);
          return;
        }
        this.setState({
          isLoaded: true,
          legs: [...this.state.legs.filter(leg => leg.id !== legId)],
        });
      })
      .catch((error) => {
        this.setState({
          isLoaded: true,
          error,
        });
        console.error("There was an error!", error);
      });
  }

  addLeg = (leg) => {
    legService.addLeg(leg)
      .then(async (response) => {
        // check for error response
        if (!response.ok) {
          // get error message from body or default to response status
          //const error = (data && data.message) || response.status;
          //return Promise.reject(error);
          return;
        }

        const id = await response.json();
        leg.id = id

        this.setState({ legs: [...this.state.legs, leg] });
      })
      .catch((error) => {
        this.setState({
          isLoaded: true,
          error,
        });
        console.error("There was an error!", error);
      });
  }

  updateLeg = (leg) => {
    legService.updateLeg(leg)
    .then(async (response) => {
      // check for error response
      if (!response.ok) {
        // get error message from body or default to response status
        //const error = (data && data.message) || response.status;
        //return Promise.reject(error);
        return;
      }

      const updatedLeg = await response.json();
      let newLegs = [...this.state.legs];
      newLegs.forEach((leg, index) => {
        if (leg.id === updatedLeg.id) {
          newLegs[index] = updatedLeg;
        }
      });
      this.setState({ legs: newLegs });
    })
    .catch((error) => {
      this.setState({
        isLoaded: true,
        error,
      });
      console.error("There was an error!", error);
    });
  }

  compare(a, b) {
    if (a.number < b.number) {
      return -1;
    }
    if (a.number > b.number) {
      return 1;
    }
    return 0;
  }

  render() {
    const { error, isLoaded, legs } = this.state;
    let content;
    if (error) {
      content = <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      content = <div>Loading...</div>;
    } else {
      content = <LegTable legs={legs} deleteLeg={this.deleteLeg} addLeg={this.addLeg} updateLeg={this.updateLeg} />;
    }

    return (
      <div>
        <Navbar />
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 mt-4">
              <div className="card bg-dark text-light">
                <div className="card-header">
                  Etappen
                </div>
                <div className="card-body" style={{padding: '0'}}>
                  {content}
                </div>
              </div>
            </div>
            <div className="col-md-6 mt-4">
              <div className="card bg-dark text-light" style={{ height: '80vh' }}>
                <div className="card-header">
                  Karte
                </div>
                <div className="card-body" style={{padding: '0'}}>
                  <GoogleMaps legs={legs} addLeg={this.addLeg}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }


}
