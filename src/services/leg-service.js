export const backendUrl = "http://ec2-18-233-150-54.compute-1.amazonaws.com/legs/";

export const getAllLegs = () => {
    return fetch(backendUrl);
}

export const addLeg = (leg) => {
    let requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(leg),
    };

    return fetch(backendUrl, requestOptions);
}

export const deleteLeg = (legId) => {
    let requestOptions = {
        method: "DELETE",
        headers: { "Content-Type": "application/json" },
        body: null,
    };
    return fetch(backendUrl + legId, requestOptions)
}

export const updateLeg = (leg) => {
    let requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(leg),
    };

    return fetch(backendUrl, requestOptions)
}